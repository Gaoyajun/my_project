#include <ros/ros.h>
#include <tf/transform_broadcaster.h>

int main(int argc, char** argv){
  ros::init(argc, argv, "my_tf_broadcaster");
  ros::NodeHandle nh;

  tf::TransformBroadcaster br;
  tf::Transform transform;

  ros::Rate rate(30);
  while (1)
  {
    ROS_INFO_STREAM("add laser and base_link frames");
    transform.setOrigin( tf::Vector3(0.0, 0.0, 0.0));
    transform.setRotation( tf::Quaternion(0, 0, 0, 1) );
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "base_link", "laser"));
   // br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "base_link", "robot"));
 //   br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "odom", "odometry_base"));
    //br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/map", "/base_link"));
    rate.sleep();
 }
  return 0;
};
